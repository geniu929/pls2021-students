#!/bin/bash

GREEN=$(tput setaf 2)
RED=$(tput setaf 1)
NORMAL=$(tput sgr0)

readonly student_file=${1:-"students.yml"}

grep ssh_pub_key "$student_file" | cut -d ':' -f 2 | while IFS= read -r sshkey
do
    if ! echo "$sshkey" | ssh-keygen -q  -l -f /dev/stdin > /dev/null
    then
        echo
        echo "${RED}[ERROR !!!!]${NORMAL} Problem with this ssh key : "
        echo "$sshkey"
        echo "Check that this is a valid ssh public key"
        echo
        exit 1
    fi
done
exit_code=$?
if [ $exit_code == 0 ] ; then
    echo "${GREEN}Ssh keys seem OK${NORMAL}"
fi
exit $exit_code
